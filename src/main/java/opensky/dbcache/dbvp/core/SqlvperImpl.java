package opensky.dbcache.dbvp.core;

import opensky.dbcache.dbvp.api.*;

import java.util.Map;

/**
 * sql解析入口实现类
 *
 * @Author zhenggm
 * @Date 2017/9/21 下午4:04
 */
public class SqlvperImpl implements Sqlvper {

    private ErrorMode mode;

    private TablePickerImpl tablePicker = new TablePickerImpl();

    public SqlvperImpl(){
        this.mode = ErrorMode.SKIP;
    }

    @Override
    public void setErrorMode(ErrorMode mode) {
        this.mode = mode;
    }

    @Override
    public String change(String sql, Map<String, Object> params) throws SqlvpException {
        // TODO 验证
        sql = tablePicker.pickTable(sql, mode,params);
        return sql;
    }
}
