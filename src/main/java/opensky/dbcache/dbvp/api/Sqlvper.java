package opensky.dbcache.dbvp.api;

import java.util.Map;

/**
 * sql文转换器
 * 根据配置的垂直拆分规则,转换sql中的库表
 *
 * @author shuim
 * @version 1.0
 */
public interface Sqlvper {
    /**
     * 设置错误模式
     *  SKIP模式:返回原sql文.
     *  THROW模式:抛出异常.
     * @param mode 错误模式
     */
    void setErrorMode(ErrorMode mode);
    /**
     * 根据配置的垂直拆分规则,转换sql中的库表
     *
     * @param sql 待转换的sql文
     * @param params 参数Map,在拆分规则的判断时可以使用
     * @return 转换完成的sql文,若是没有发生转换则返回传入的sql文
     * @throws SqlvpException THROW模式下,转换错误
     */
    String change(String sql, Map<String, Object> params);
}
