package opensky.dbcache.dbvp.api;


/**
 * 拆分规则
 */
public interface PartionRule extends Ruler {
    /**
     * 获取schema的匹配类型
     */
    MapType getSchemaMapType();

    /**
     * 获取schema
     */
    String getSchema();

    /**
     * 获取table的匹配类型
     */
    MapType getTableMapType();

    /**
     * 获取table
     */
    String getTable();

    /**
     * 匹配规则
     */
    boolean map(FullTableName table);

    /**
     * 转换成功后,是否传递给之后的rule
     */
    boolean isPassOn();
}



