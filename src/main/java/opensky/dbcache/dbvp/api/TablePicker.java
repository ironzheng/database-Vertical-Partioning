package opensky.dbcache.dbvp.api;


import java.util.Map;

/**
 * 库表挑选器
 * 从sql文中挑选出所有的库表
 *
 * @author shuim
 * @version 1.0
 */
public interface TablePicker {
    /**
     * 从sql文中挑选出所有的库表
     *
     * @param sql sql文
     * @param mode 错误的策略模式
     * @param params
     * @return 库表列表
     * @throws SqlvpException
     */


    String pickTable(String sql, ErrorMode mode, Map<String, Object> params);
}
