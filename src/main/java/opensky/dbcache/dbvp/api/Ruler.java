package opensky.dbcache.dbvp.api;

import java.text.ParseException;
import java.util.Map;
/**
 * 规则表转换接口
 *
 * @author shuim
 * @version 1.0
 */
public interface Ruler {

    /**
     * 转换表
     *
     * @param  table 待转换的表
     * @param  params 转换时可用的参数
     * @return 转换后的表,没有转换时返回NULL
     * @throws SqlvpException
     */
    FullTableName change(FullTableName table, Map<String, Object> params) throws ParseException;
}
