package opensky.dbcache.dbvp.api;

/**
 * 匹配类型
 *
 * @author shuim
 * @version 1.0
 */
public enum MapType {
    /**
     * 相等
     */
    EQUAL("equal"),
    /**
     * 正则
     */
    REG("regexp");
    private String value;

    MapType(String value) {
        this.value = value;
    }

    public static MapType getByValue(String value) {
        if (value != null) {
            for (MapType e : values()) {
                if (e.value.equals(value)) {
                    return e;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return value;
    }
}
