package opensky.dbcache.dbvp.api;

/**
 * 垂直拆分异常
 *
 * @author shuim
 * @version 1.0
 */
public class SqlvpException extends RuntimeException {
    private static final long serialVersionUID = -161563220016964450L;

    public SqlvpException() {
        super();
    }

    public SqlvpException(String message) {
        super(message);
    }

    public SqlvpException(String message, Throwable cause) {
        super(message, cause);
    }

    public SqlvpException(Throwable cause) {
        super(cause);
    }

    protected SqlvpException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
