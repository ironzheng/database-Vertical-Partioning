package opensky.dbcache.dbvp.api;

/**
 * 垂直拆分sql转变时,发生异常的处理模式
 *
 * @author shuim
 * @version 1.0
 */
public enum ErrorMode {
    SKIP("skip"), THROW("throw");
    private String value;

    ErrorMode(String value) {
        this.value = value;
    }

    public static ErrorMode getByValue(String value) {
        if (value != null) {
            for (ErrorMode e : values()) {
                if (e.value.equals(value)) {
                    return e;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return value;
    }
}
