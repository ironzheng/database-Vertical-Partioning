package opensky.dbcache.dbvp.rule;


/**
 * groovy脚本拼接字符
 * @Author zhenggm
 * @Date 2017/9/21 下午4:04
 */
public class PartionString {

    public static String getClassContext(String shell) {
        StringBuffer sb = new StringBuffer();
        sb.append("import opensky.dbcache.dbvp.api.FullTableName;\n" +
                "import opensky.dbcache.dbvp.api.MapType;\n" +
                "import opensky.dbcache.dbvp.api.PartionRule;\n" +
                "import opensky.dbcache.dbvp.rule.MatchType;\n" +
                "import opensky.dbcache.dbvp.rule.Param;\n" +
                "import opensky.dbcache.dbvp.rule.RegExp;\n" +
                "import opensky.dbcache.dbvp.rule.Rule;\n" +
                "import org.slf4j.Logger;\n" +
                "import org.slf4j.LoggerFactory;\n" +
                "\n" +
                "import java.text.ParseException;\n" +
                "import java.text.SimpleDateFormat;\n" +
                "import java.util.Date;\n" +
                "import java.util.List;\n" +
                "import java.util.Map;\n" +
                "import java.util.regex.Matcher;\n" +
                "import java.util.regex.Pattern;\n" +
                "\n" +
                "public class PartionRuleImpl implements PartionRule {\n" +
                "\n" +
                "    private static Logger logger = LoggerFactory.getLogger(PartionRuleImpl.class);\n" +
                "\n" +
                "    private Rule rule;\n" +
                "\n" +
                "\n" +
                "    @Override\n" +
                "    public MapType getSchemaMapType() {\n" +
                "        return rule.getSchemaType();\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public String getSchema() {\n" +
                "        return rule.getSchema();\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public MapType getTableMapType() {\n" +
                "        return rule.getTableType();\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public String getTable() {\n" +
                "        return rule.getTable();\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public boolean map(FullTableName table) {\n" +
                "        boolean s = false;\n" +
                "        boolean t = false;\n" +
                "        if (rule.getSchemaType().equals(MapType.EQUAL)) {\n" +
                "            // schema是精确匹配\n" +
                "            s = table.getSchema().equals(rule.getSchema());// 直接比较是否相等\n" +
                "            if (rule.getTableType().equals(MapType.EQUAL)) {\n" +
                "                // table是精确匹配\n" +
                "                t = table.getTable().equals(rule.getTable());\n" +
                "            } else {\n" +
                "                // table是非精确匹配\n" +
                "                // 判断传入table是否匹配规则中的正则表达式\n" +
                "                t = this.regxStr(table.getTable(), rule.getTable());\n" +
                "            }\n" +
                "        } else {\n" +
                "            // schema是非精确匹配，正则\n" +
                "            s = this.regxStr(table.getSchema(), rule.getSchema());\n" +
                "            if (rule.getTableType().equals(MapType.EQUAL)) {\n" +
                "                t = table.getTable().equals(rule.getTable());\n" +
                "            } else {\n" +
                "                // 判断传入schema是否匹配规则中的正则表达式\n" +
                "                t = this.regxStr(table.getTable(), rule.getTable());\n" +
                "            }\n" +
                "        }\n" +
                "        // 只有当s和t都匹配上的时候\n" +
                "        if (s == true && t == true) {\n" +
                "            return true;\n" +
                "        }\n" +
                "        return false;\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public boolean isPassOn() {\n" +
                "        return rule.isProcess();\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public FullTableName change(FullTableName table, Map<String, Object> context) throws ParseException {");

        sb.append(
                " Boolean flag = false;\n" +
                        "flag = matchParam(table, context);\n" +
                        "\n" +
                        "        // todo 匹配成功之后进行替换\n" +
                        "        if (flag) {\n" +
                        "            // 替换${this}为当前schema和table\n" +
                        "            java.lang.String s = rule.getFullTableName().getSchema();\n" +
                        "            java.lang.String t = rule.getFullTableName().getTable();\n" +
                        "            List<String> schemalist = RegExp.getKeywords(s);\n" +
                        "            List<String> tablelist = RegExp.getKeywords(t);\n" +
                        "            if (schemalist.size() > 0) {\n" +
                        "                for (String n : schemalist) {\n" +
                        "                    if (n.equals(\"schema\")) {\n" +
                        "            s = s.replace('${schema}', table.getSchema());\n" +
                        "                    }\n" +
                        "                }\n" +
                        "            }\n" +
                        "            if (tablelist.size() > 0) {\n" +
                        "                for (String n : tablelist) {\n" +
                        "                    if (n.equals(\"table\")) {\n" +
                        "                        t = t.replace('${table}', table.getTable());\n" +
                        "                    }\n" +
                        "                }\n" +
                        "            }\n" +
                        "            // 替换{v1}中的所有v。\n" +
                        "            if (!context.isEmpty() && context != null) {\n" +
                        "                for (Map.Entry<String, Object> entry : context.entrySet()) {\n" +
                        "                    s = s.replace(\"{\" + entry.getKey() + \"}\", String.valueOf(entry.getValue()));\n" +
                        "                    t = t.replace(\"{\" + entry.getKey() + \"}\", String.valueOf(entry.getValue()));\n" +
                        "                }\n" +
                        "            }\n" +
                        "\n" +
                        "            // 返回替换之后的结果。\n" +
                        "            return new FullTableName(s,t);\n" +
                        "        }\n" +
                        "        return null;" +
                        "}\n" +
                        "// 默认属性匹配方法（equals,isNull,less,greater）\n" +
                        "    private boolean matchParam(FullTableName table, Map<String, Object> context) throws ParseException {");
        sb.append(shell);
        sb.append("\n" +
                "}\n" +
                "    private boolean regxStr(String str, String regx) {\n" +
                "        Pattern p = Pattern.compile(regx);\n" +
                "        Matcher m = p.matcher(str);\n" +
                "        return m.matches();\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    public Rule getRule() {\n" +
                "        return rule;\n" +
                "    }\n" +
                "\n" +
                "    public void setRule(Rule rule) {\n" +
                "        this.rule = rule;\n" +
                "    }" +
                "}");

        return sb.toString();
    }
}
