package opensky.dbcache.dbvp.rule;


import opensky.dbcache.dbvp.api.FullTableName;
import opensky.dbcache.dbvp.api.MapType;

import java.util.Map;

/**
 * 规则基础字段
 *
 * @User: zhenggm
 * @Date: 2017/9/18 下午2:19
 */
public class Rule {
    private String schema;
    private String table;
    private MapType schemaType;
    private MapType tableType;
    private boolean isProcess;// 是否继续按新字段继续遍历Set
    private FullTableName fullTableName;
    private Map<String,Param> map;// rule中所有的param属性

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public MapType getSchemaType() {
        return schemaType;
    }

    public void setSchemaType(MapType schemaType) {
        this.schemaType = schemaType;
    }

    public MapType getTableType() {
        return tableType;
    }

    public void setTableType(MapType tableType) {
        this.tableType = tableType;
    }

    public boolean isProcess() {
        return isProcess;
    }

    public void setProcess(boolean process) {
        isProcess = process;
    }

    public FullTableName getFullTableName() {
        return fullTableName;
    }

    public void setFullTableName(FullTableName fullTableName) {
        this.fullTableName = fullTableName;
    }

    public Map<String, Param> getMap() {
        return map;
    }

    public void setMap(Map<String, Param> map) {
        this.map = map;
    }
}
