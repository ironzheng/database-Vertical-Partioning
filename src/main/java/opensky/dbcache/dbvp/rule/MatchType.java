package opensky.dbcache.dbvp.rule;


/**
 * rule中param匹配规则方法
 * @Author zhenggm
 * @Date 2017/9/21 下午4:04
 */
public enum MatchType {
    EQUALS("equals"),ISNULL("isNull"),GREATER("greater"),LESS("less");

    private String code;

    public static MatchType getByValue(String value) {
        if (value != null) {
            for (MatchType e : values()) {
                if (e.code.equals(value)) {
                    return e;
                }
            }
        }
        return null;
    }

    MatchType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
